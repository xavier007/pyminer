# import pretty_errors
import time

t0 = time.time()
from pyminer2.pmappmodern import main
import tornado.platform.asyncio as async_io
import sys

if sys.platform == 'win32':
    async_io.asyncio.set_event_loop_policy(async_io.asyncio.WindowsSelectorEventLoopPolicy())
t2 = time.time()
print('importing_time', t2 - t0)
main()
